// useful to have them as global variables
let canvas, ctx, w, h; 
let mousePos;
// an empty array!
let balls = [];
let enemyBalls=[{
  x: w/2,
  y: h/2,
  radius: 0,
  speedX: 0,
  speedY: 0,
  color: 'black',
}];
let HPIndicator,LVLIndicator;
const player = {
  name:'',
  lvl:1,
  hp:3,
  x: 10,
  y: 10,
  width: 20,
  height: 20,
  color: 'red'
}

let ballsCountToWin='',newBallsCountToWin,playerSize,minBallSize,maxBallSize,maxBallSpeed
function checkForm(){
  if(player.name===''){
    player.name="Default Name"
  }
  if(playerSize!=='' || playerSize>0){
    player.width=playerSize
    player.height=playerSize
  }
  if(ballsCountToWin===''|| ballsCountToWin<=0){
    ballsCountToWin=10
  }
  if(minBallSize===''|| minBallSize<=5){
    minBallSize=5
  }
  if(maxBallSize===''|| maxBallSize<=5 || maxBallSize>50){
    maxBallSize=30;
  }
  if(maxBallSpeed===''|| maxBallSpeed<5){
    maxBallSpeed=15;
  }

}

function init() {
  player.name=document.form1.elements.namedItem('playerName').value;
  ballsCountToWin=document.form1.elements.namedItem('ballsCountToWin').value;
  playerSize=document.form1.elements.namedItem('playerSize').value;
  minBallSize=document.form1.elements.namedItem('minSize').value;
  maxBallSize=document.form1.elements.namedItem('maxSize').value;
  maxBallSpeed=document.form1.elements.namedItem('maxBallSpeed').value;
  checkForm();


  enemyBalls=[{x: w/2, y: h/2, radius: 0, speedX: 0, speedY: 0, color: 'black',}];
  canvas = document.querySelector("#myCanvas");
  newBallsCountToWin=ballsCountToWin;

  // often useful
  w = canvas.width; 
  h = canvas.height;
  // important, we will draw with this object
  ctx = canvas.getContext('2d');
  let countOfBalls=10
  // create 10 balls
  balls = createBalls(countOfBalls);

  // add a mousemove event listener to the canvas
  canvas.addEventListener('mousemove', mouseMoved);

  HPIndicator= document.getElementById("playerHealth");
  LVLIndicator= document.getElementById("playerLevel");
  // ready to go !

  mainLoop()

};


function mouseMoved(evt) {
  mousePos = getMousePos(canvas, evt);
}

function getMousePos(canvas, evt) {
  // necessary work in the canvas coordinate system
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function movePlayerWithMouse() {
  if(mousePos !== undefined) {
    player.x = mousePos.x;
    player.y = mousePos.y;
  }
}
function checkPlayerHealth(){
  if(player.hp===0){
    ctx.font="30px Arial";
    ctx.fillText("YOU LOSE!", 20, 30);
    return false;
  }
  return true;
}
let friendlyBallCounter=0,enemyBallCounter=0;
function mainLoop() {
  // 1 - clear the canvas
  ctx.clearRect(0, 0, w, h);

  HPIndicator.innerHTML=HPIndicator.textContent="HP:"+player.hp;
  LVLIndicator.innerHTML=LVLIndicator.textContent="Level:"+player.lvl;
  if(!checkPlayerHealth()){
    sendRequest('post', {name:player.name,level:player.lvl})
        .then(data=>sendRequest('get')
            .then(data=>console.log(data))
        )
        .catch(err=>console.log(err))
    return false;
  }
  if(drawNumberOfBallsToWin()){
    enemyBalls=[{x: w/2, y: h/2, radius: 0, speedX: 0, speedY: 0, color: 'black',}];
   // balls = createBalls(10);
    player.lvl+=1;
    player.hp=3;
    newBallsCountToWin+=5;
    ballsCountToWin=newBallsCountToWin;
  }
  // draw the ball and the player
  drawFilledRectangle(player);
  drawBalls(balls);
  drawBalls(enemyBalls);
  // animate the ball that is bouncing all over the walls
  moveAllBalls(balls,enemyBalls);
  movePlayerWithMouse();

  friendlyBallCounter+=1;
  enemyBallCounter+=1;
  if(friendlyBallCounter==60){
      addFriendlyBalls();
      friendlyBallCounter=0;
  }
  if(enemyBallCounter==200){
    addEnemyBalls();
    enemyBallCounter=0;
  }
  // ask for a new animation frame
  requestAnimationFrame(mainLoop);
}

// Collisions between rectangle and circle
function circRectsOverlap(x0, y0, w0, h0, cx, cy, r) {
  let testX = cx;
  let testY = cy;
  if (testX < x0) testX = x0;
  if (testX > (x0 + w0)) testX = (x0 + w0);
  if (testY < y0) testY = y0;
  if (testY > (y0+h0)) testY = (y0 + h0);
  return (((cx - testX) * (cx - testX) + (cy - testY) * (cy - testY)) <  r * r);
}
function createBalls(n) {
  // empty array
  const ballArray = [];
  
  // create n balls
  for(let i=0; i < n; i++) {
    const b = {
      x: w/2,
      y: h/2,
      radius: minBallSize + maxBallSize * Math.random(), // between 5 and 35
      speedX: -5 + maxBallSpeed * Math.random(), // between -5 and + 5
      speedY: -5 + maxBallSpeed * Math.random(), // between -5 and + 5
      color: getARandomColor(),
    }
    // add ball b to the array
     ballArray.push(b);
  }
  // returns the array full of randomly created balls
  return ballArray;
}
function addFriendlyBalls(){
  const b = {
    x: w/2,
    y: h/2,
    radius: minBallSize + maxBallSize * Math.random(),
    speedX: -5 + maxBallSpeed * Math.random(),
    speedY: -5 + maxBallSpeed * Math.random(),
    color: getARandomColor(),
  }
  balls.push(b)
}
function addEnemyBalls(){
  const b = {
    x: w/2,
    y: h/2,
    radius: 20,
    speedX: -5 + maxBallSpeed * Math.random(),
    speedY: -5 + maxBallSpeed * Math.random(),
    color: 'black',
  }
  enemyBalls.push(b)
}
function getARandomColor() {
  const colors = ['red', 'blue', 'cyan', 'purple', 'pink', 'green', 'yellow'];
  // a value between 0 and color.length-1
  // Math.round = rounded value
  // Math.random() a value between 0 and 1
  let colorIndex = Math.round((colors.length-1) * Math.random()); 
  let c = colors[colorIndex];
  
  // return the random color
  return c;
}

//function drawNumberOfBallsAlive(balls) {
//  ctx.save();
//  ctx.font="30px Arial";
//
//  if(balls.length === 0) {
//    ctx.fillText("YOU WIN!", 20, 30);
//  } else {
//    ctx.fillText(balls.length, 20, 30);
//      }
//  ctx.restore();
//}
function drawNumberOfBallsToWin() {
  ctx.save();
  ctx.font="30px Arial";
  if(ballsCountToWin === 0) {
    ctx.fillText("YOU WIN!", 20, 30);
    return true;
  } else {
  ctx.fillText(ballsCountToWin, 20, 30);
  return false;
      }
  ctx.restore();
}

function drawBalls(ballArray) {
  ballArray.forEach(function(b) {
    drawFilledCircle(b);
  });
}
function moveAllBalls(ballArray,enemyBallArray) {
  // iterate on all balls in array
  ballArray.forEach(function(b, index) {
    // b is the current ball in the array
    b.x += b.speedX;
    b.y += b.speedY;
    testCollisionBallWithWalls(b); 
    testCollFriendlyBallWithPlayer(b, index);
  });
  enemyBallArray.forEach(function(b, index) {
    b.x += b.speedX;
    b.y += b.speedY;
    testCollisionBallWithWalls(b);
    testCollEnemyBallWithPlayer(b, index);
  });
}
function testCollEnemyBallWithPlayer(b, index) {
  if(circRectsOverlap(player.x, player.y,
      player.width, player.height,
      b.x, b.y, b.radius)) {
    enemyBalls.splice(index, 1);
    player.hp-=1;
  }
}
function testCollFriendlyBallWithPlayer(b, index) {
  if(circRectsOverlap(player.x, player.y,
                     player.width, player.height,
                     b.x, b.y, b.radius)) {
    // we remove the element located at index
    // from the balls array
    // splice: first parameter = starting index
    //         second parameter = number of elements to remove
    balls.splice(index, 1);
    ballsCountToWin-=1;
  }
}

function testCollisionBallWithWalls(b) {
  // COLLISION WITH VERTICAL WALLS ?
  if((b.x + b.radius) > w) {
    // the ball hit the right wall
    // change horizontal direction
    b.speedX = -b.speedX;
    
    // put the ball at the collision point
    b.x = w - b.radius;
  } else if((b.x -b.radius) < 0) {
    // the ball hit the left wall
    // change horizontal direction
    b.speedX = -b.speedX;
    
    // put the ball at the collision point
    b.x = b.radius;
  }
 
  // COLLISIONS WTH HORIZONTAL WALLS ?
  // Not in the else as the ball can touch both
  // vertical and horizontal walls in corners
  if((b.y + b.radius) > h) {
    // the ball hit the right wall
    // change horizontal direction
    b.speedY = -b.speedY;
    
    // put the ball at the collision point
    b.y = h - b.radius;
  } else if((b.y -b.radius) < 0) {
    // the ball hit the left wall
    // change horizontal direction
    b.speedY = -b.speedY;
    
    // put the ball at the collision point
    b.Y = b.radius;
  }  
}

function drawFilledRectangle(r) {
  // GOOD practice: save the context, use 2D trasnformations
  ctx.save();
  
  // translate the coordinate system, draw relative to it
  ctx.translate(r.x, r.y);
  
  ctx.fillStyle = r.color;
  // (0, 0) is the top left corner of the monster.
  ctx.fillRect(-10,-10 , r.width, r.height);
  
  // GOOD practice: restore the context
  ctx.restore();
}

function drawFilledCircle(c) {
  // GOOD practice: save the context, use 2D trasnformations
  ctx.save();
  
  // translate the coordinate system, draw relative to it
  ctx.translate(c.x, c.y);
  
  ctx.fillStyle = c.color;
  // (0, 0) is the top left corner
  ctx.beginPath();
  ctx.arc(0, 0, c.radius, 0, 2*Math.PI);
  ctx.fill();
 
  // GOOD practice: restore the context
  ctx.restore();
}

function sendRequest(method,form=null){
  switch (method){
    case 'get':
      return fetch('http://localhost:3000/qwerty').then(response=>{
        return response.json();
      })
    case 'post':
      return fetch('http://localhost:3000/qwerty',
          {method:'post',
            body:JSON.stringify(form),
            headers: {'Content-Type': 'application/json'}
          }).then(response=>{
        return response.json();
      })

  }
}